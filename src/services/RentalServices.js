import axios from 'axios';

const url = 'https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json';

export async function RentalService() {
    try {
        const response = await axios.get(url)
        
        return response;
    } catch (error) {
        console.log(error);
    }
}
