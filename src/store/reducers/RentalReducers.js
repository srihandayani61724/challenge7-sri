import { GET_CARS, SEARCH_CARS } from "../actions";

const rentalReducers = (state = {data: []}, action) => {
    switch (action.type) {
        case GET_CARS:
            return {...state,data: action.payload}
        case SEARCH_CARS:
            return {...state,data: action.payload}
        default:
            return state 
    }
}

export default rentalReducers
