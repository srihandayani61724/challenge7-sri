import { combineReducers } from "redux";

import rentalReducers from "./RentalReducers";

const reducers = combineReducers({
    data: rentalReducers

})

export default reducers