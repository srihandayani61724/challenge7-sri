import React from 'react';
import ImgFooter1 from '../assets/images/img/icon_facebook.png'
import ImgFooter2 from '../assets/images/img/icon_instagram.png'
import ImgFooter3 from '../assets/images/img/icon_twitter.png'
import ImgFooter4 from '../assets/images/img/icon_mail.png'
import ImgFooter5 from '../assets/images/img/icon_twitch.png'

const Footer = () => {
    return (
        <>
            <footer id="footer">
                <div class="row">
                    <div class="col-lg-3" id="address">
                        <p>Jalan Suroyo No. 161 Mayangan Kota <br />Probolonggo 672000</p>
                        <p>binarcarrental@gmail.com</p>
                        <p>081-233-334-808</p>
                    </div>
                    <div class="col-lg-3" id="navigation-footer">
                        <ul>
                            <a href="#our_services">Our Services</a>
                        </ul>
                        <ul>
                            <a href="#why_us">Why Us</a>
                        </ul>
                        <ul>
                            <a href="#testimonial">Testimonial</a>
                        </ul>
                        <ul>
                            <a href="#frequently-asked">FAQ</a>
                        </ul>
                    </div>
                    <div class="col-lg-3" id="connect-with-us">
                        <p>Connect with us</p>
                        <img className="ms-1" src={ImgFooter1} />
                        <img className="ms-1" src={ImgFooter2} />
                        <img className="ms-1" src={ImgFooter3} />
                        <img className="ms-1" src={ImgFooter4} />
                        <img className="ms-1" src={ImgFooter5} />
                    </div>
                    <div class="col-lg-3" id="copyright">
                        <p>Copyright Binar 2022</p>
                        <svg width="100" height="34" viewBox="0 0 100 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="100" height="34" fill="#0D28A6" />
                        </svg>
                    </div>
                </div>
            </footer>
        </>
    );
}

export default Footer;
