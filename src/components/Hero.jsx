import React from 'react';
import IMGHero from '../assets/images/img/img_car.png'
import { Link } from 'react-router-dom';


const Hero = () => {
    return (
        <>
            <div id="hero">
                <div class="row">
                    <div class="col-6 tagline align-items-center">
                        <h2>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h2>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
                            terbaik
                            dengan harga terjangkau. Selalu
                            siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                        <Link to="/cars"><button type="button" class="btn btn-success">Mulai
                                Sewa Mobil</button></Link>
                    </div>
                    <div class="col-6 mt-auto image-tagline">
                        <img src={IMGHero} alt="Mobil Merah" width="100%" />
                    </div>
                </div>
            </div>
        </>
    );
}

export default Hero;
